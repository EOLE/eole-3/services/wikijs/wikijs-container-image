# syntax=docker/dockerfile:1

FROM ghcr.io/linuxserver/baseimage-alpine:3.20

# set version label
ARG BUILD_DATE
ARG VERSION
ARG WIKIJS_RELEASE
LABEL build_version="Linuxserver.io version:- ${VERSION} Build-date:- ${BUILD_DATE}"
LABEL maintainer="alex-phillips"

# environment settings
ENV HOME="/app"
ENV NODE_ENV="production"

COPY app-version /tmp
RUN \
  echo "**** install build packages ****" && \
  apk -U --update --no-cache add --virtual=build-dependencies \
    build-base \
    python3 && \
  apk add --no-cache \
    git \
    nodejs \
    npm \
    openssh && \
  echo "**** install wiki.js ****" && \
  mkdir -p /app/wiki && \
  curl -o \
    /tmp/wiki.tar.gz -L \
    "https://github.com/Requarks/wiki/releases/download/$(cat /tmp/app-version)/wiki-js.tar.gz" && \
  tar xf \
    /tmp/wiki.tar.gz -C \
    /app/wiki/ && \
  cd /app/wiki && \
  npm rebuild sqlite3 && \
  echo "**** cleanup ****" && \
  apk del --purge \
    build-dependencies && \
  rm -rf \
    $HOME/.cache \
    /tmp/*

# copy local files
COPY root/ /

# ports and volumes
EXPOSE 3000

VOLUME /config
